import sys
from pathlib import Path

import modeller
import numpy as np
from modeller.automodel import automodel, dopehr_loopmodel, refine

from sts_cation_prediction import helper
from sts_cation_prediction.helper import path_type

MODELLER_LIB = "" # Replace with path to modlib (miniconda3/lib/modeller-9.20/modlib)


def align_templates_to_sequence(sequence_file: path_type, templates_file: path_type, alignment_folder: path_type, hmm_file: path_type=None) -> path_type:
    """
    clustalo align sequence to template sequences

    Parameters
    ----------
    sequence_file
        file containing a single sequence
    templates_file
        subsequence file of templates
    alignment_folder
        where to store the output file
    hmm_file
        use hmm to align

    Returns
    -------
    alignment filename
    """
    _, sequence_name, _ = helper.get_file_parts(sequence_file)
    _, templates_name, _ = helper.get_file_parts(templates_file)
    sequences = helper.get_sequences_from_fasta(sequence_file)
    template_sequences = helper.get_sequences_from_fasta(templates_file)
    unaligned_file = Path(alignment_folder) / f"{sequence_name}_{templates_name}.fasta"
    with open(unaligned_file, "w") as f:
        for key in sequences:
            f.write(f">{key}\n{sequences[key]}\n")
        for key in template_sequences:
            f.write(f">{key}\n{template_sequences[key]}\n")
    aligned_file = Path(alignment_folder) / f"{sequence_name}_{templates_name}_aln.fasta"
    helper.clustal_msa_from_sequences(str(unaligned_file), aligned_file, input_hmm_file=hmm_file)
    return aligned_file


def convert_fasta_to_ali(sequence_name: str, fasta_file: path_type, templates_start_end: dict, ali_file: path_type=None) -> path_type:
    """
    Converts fasta of sequence and templates to ali format

    Parameters
    ----------
    sequence_name
        name of sequence, rest are assumed as template structures
    fasta_file
    ali_file
    templates_start_end
        dict with start and end residue identifiers and chains for each template

    Returns
    -------
    ali filename
    """
    if ali_file is None:
        path, name, _ = helper.get_file_parts(fasta_file)
        ali_file = Path(path) / f"{name}.ali"
    sequences = helper.get_sequences_from_fasta(fasta_file)
    with open(ali_file, "w") as f:
        for key in sequences:
            if key == sequence_name:
                f.write(f">P1;{key}\nsequence:{key}:::::::0.00: 0.00\n{sequences[key].upper()}*\n")
            else:
                start_chain, start_pos, end_chain, end_pos = templates_start_end[key]
                f.write(f">P1;{key}\nstructureX:{key}:{start_pos}:{start_chain}:{end_pos}:{end_chain}:::0.00: 0.00\n{sequences[key].upper()}*\n")
    return ali_file


def make_env(pdb_directory: path_type, rand_seed: int=5):
    """
    Make a modeller environ with a random seed.
    Change seed if parallelizing.

    Parameters
    ----------
    pdb_directory
    rand_seed

    Returns
    -------
    environ

    """
    env = modeller.environ(rand_seed=rand_seed)
    env.io.atom_files_directory = [str(pdb_directory)]
    env.io.hetatm = True
    return env


def get_ndope_score(pdb_file: path_type) -> float:
    """
    Retrieves n-DOPE score from PDB file

    Parameters
    ----------
    pdb_file

    Returns
    -------
    score
    """
    with open(pdb_file) as f:
        for line in f:
            if "Normalized DOPE score" in line:
                score = line.split(":")[-1].strip()
                return float(score)


def model(name: str, templates: list, ali_file: path_type, pdb_dir: path_type, model_path: path_type, rand_seed: int, num_models: int=500, loop_refine: bool=False):
    """
    Makes models for name.
    DELETES all intermediate files
    MOVES final model pdbs and log file to model_path

    Parameters
    ----------
    name
    templates
    ali_file
    model_path
    pdb_dir
    rand_seed
    num_models
    loop_refine
    """
    env = make_env(pdb_dir, rand_seed)
    if loop_refine:
        a = dopehr_loopmodel(env,
                             alnfile=str(ali_file),
                             knowns=tuple(templates),
                             sequence=name,
                             assess_methods=(modeller.automodel.assess.DOPE,
                                             modeller.automodel.assess.normalized_dope))
        a.md_level = None
        a.loop.starting_model = 1
        a.loop.ending_model = 5
        a.loop.md_level = refine.fast
    else:
        a = automodel(env,
                      alnfile=str(ali_file),
                      knowns=tuple(templates),
                      sequence=name,
                      assess_methods=(modeller.automodel.assess.DOPE,
                                      modeller.automodel.assess.normalized_dope))
    a.starting_model = 1
    a.ending_model = num_models
    a.make()
    files = Path.cwd().glob(name + ".*")
    log_file = Path(model_path) / f"{name}.log"
    with open(log_file, "w") as f:
        for file_path in files:
            _, filename, ext = helper.get_file_parts(file_path)
            if ext == ".pdb":
                f.write(f"{filename}\t{get_ndope_score(file_path)}\n")
                new_file = Path(model_path) / f"{filename}{ext}"
                file_path.rename(new_file)
            else:
                file_path.unlink()


def get_ndope_score_expensive(model_file):
    """
    Get NDOPE score by running MODELLER assess_normalized_dope()

    Parameters
    ----------
    model_file

    Returns
    -------
    NDOPE
    """
    model_file = str(model_file)
    env = modeller.environ()
    env.libs.topology.read(file=f'{MODELLER_LIB}/top_heav.lib')  # read topology
    env.libs.parameters.read(file=f'{MODELLER_LIB}/par.lib')  # read parameters
    mdl = modeller.scripts.complete_pdb(env, model_file)
    return mdl.assess_normalized_dope()


def parse_log_file(input_model_dir: path_type, log_file: path_type, ignore_none=True):
    """
    Parses a log file to get the NDOPE scores of each pdb file
    If the score is None, it tries to get te score directly from the PDB file
    If this doesn't work and ignore_none is False
        then it runs MODELLER on the PDB file to get the NDOPE scores (this takes longer)
    Parameters
    ----------
    input_model_dir
    log_file
    ignore_none

    Returns
    -------

    """
    filenames, scores = [], []
    with open(log_file) as f:
        for line in f:
            name, score = line.strip().split("\t")
            if score == 'None':
                filename = Path(input_model_dir) / f"{name}.pdb"
                score = get_ndope_score(filename)
                if score is None:
                    if ignore_none:
                        continue
                    else:
                        try:
                            score = get_ndope_score_expensive(filename)
                            with open(filename) as f1:
                                lines = f1.readlines()
                            with open(filename, 'w') as f1:
                                for i, l in enumerate(lines):
                                    if i == 2:
                                        f1.write(f"REMARK   6 Normalized DOPE score: {score}\n")
                                    f1.write(l)
                        except modeller.FileFormatError:
                            continue
            filenames.append(name)
            scores.append(float(score))
    sorted_scores = np.argsort(scores)
    return filenames, scores, sorted_scores


def get_ndope_profile(model_file):
    """
    Get NDOPE per residue

    Parameters
    ----------
    model_file

    Returns
    -------
    NDOPE per residue
    """
    env = modeller.environ()
    env.libs.topology.read(file=f'{MODELLER_LIB}/top_heav.lib')  # read topology
    env.libs.parameters.read(file=f'{MODELLER_LIB}/par.lib')  # read parameters
    mdl = modeller.scripts.complete_pdb(env, str(model_file))
    return mdl.get_normalized_dope_profile()


def main():
    """
    Makes homology models for a single key
    Takes command line arguments
    key, index, template_dir, ali_dir, model_dir
    RUN USING parallel_functions.py

    """
    arguments = sys.argv
    assert len(arguments) == 6
    key, index, template_dir, ali_dir, model_dir = arguments[1:]
    template_dir = Path(template_dir)
    ali_dir = Path(ali_dir)
    model_dir = Path(model_dir)
    templates = [helper.get_file_parts(filename)[1] for filename in template_dir.glob("*.pdb")]
    ali_file = ali_dir / f"{key}_templates_c_aln.ali"
    if not ali_file.exists():
        print(f"{ali_file} missing")
    if ali_file.exists() and not (model_dir / f"{key}.log").exists():
        model(key, templates, ali_file, template_dir, model_dir, int(index), num_models=250, loop_refine=False)


if __name__ == '__main__':
    main()
