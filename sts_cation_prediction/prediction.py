import pickle
import re
from collections import defaultdict, Counter
from pathlib import Path

import matplotlib.pyplot as plt
import numba as nb
import numpy as np
import plotly.graph_objs as go
from ete3 import NCBITaxa
from sklearn import metrics, preprocessing
from sklearn import model_selection
from sklearn.decomposition import PCA
from xgboost.sklearn import XGBClassifier

from sts_cation_prediction.sequence_feature_extraction import STRING_MAPPINGS

NCBI = NCBITaxa()

SCORE_NAMES = ["f1", "balanced_accuracy", "roc_auc", "precision_recall_auc"]
MOTIFS = ["R.[RQ]", "DD..[DE]|ND..[DE]|DN..D", "[ND]D..[STG]...[EX]|DD..[DE]"]
ALN_MOTIFS = ["R-*[^-]-*[RQ]", "D-*D-*[^-]-*[^-]-*[DE]|N-*D-*[^-]-*[^-]-*[DE]|D-*N-*[^-][^-]-*D",
              "[ND]-*D-*[^-]-*[^-]-*[STG]-*[^-]-*[^-]-*[^-]-*[EX]|D-*D-*[^-]-*[^-]-*[DE]"]
PATTERNS = (re.compile(motif) for motif in MOTIFS)
ALN_PATTERNS = (re.compile(motif) for motif in ALN_MOTIFS)
PRODUCT_GROUPING = {"F": ([], {
        "10,1": (
            ["germacrene b", "germacrene a", "avermitilol", "patchoulol", "bicyclo", "sibir", "aroma", "helmintho", "elemol", "hedycar", "eudesmol"],
            {
                "1,2H": (["germacrene c", "guaia", "guaio", "kunz"], {
                    "germacrene C": (["delta-selinene"], {}),
                    "1,2H": (["(+)-germacrene d"], {})
                }),
                "1,3H": (["valer", "germacrene-d", "germacrene d", "germacradienol", "dien-4-ol", "D-4-ol"], {}),
                "germacrene A": (["selin", "aristolo", "eremoph", "valen", "vetispir", "guaiene", "bulne", "elem"], {}),
                "bicyclogermacrene": (["gurju", "viridi"], {}),
            }),
        "11,1": (["humul", "hyem", "zerumb", "caryop", "protoillu", "isocom", "pentalen"], {}),
        "acyclic": (["farnesene", "farnesol"], {})
    }),
              "N": ([], {
                  "acyclic": (["nerolidol"], {}),
                  "11,1": (["longi", "himachal"], {}),
                  "6,1": (
                      ["bisabol", "itali", "sesquiphell", "sesquisab", "curcum", "zingi", "thuj", "santal", "cupre", "macrocarp", "chamig", "bergam",
                       "amorpha", "isoziz", "cedrol", "barba", "koidz"], {})
              }),
              "cadalanes": (["cadin", "cube", "amorphe", "sativ", "muuro", "copa", "ylang"], {})
              }


def get_product_group_graph(product: str) -> list:
    """
    Gets intermediate tree of a product from the (F)arneyl or (N)eroildyl cation
    
    Parameters
    ----------
    product: name of product

    Returns
    -------
    list of intermediates
    """
    product = product.lower()
    def is_product_in_group(p, group):
        for string in group:
            if string in p:
                return True
        return False
    def get_match(p, groups, graph):
        for parent in groups:
            if is_product_in_group(p, groups[parent][0]):
                return graph + [parent]
            else:
                result = get_match(p, groups[parent][1], graph + [parent])
                if result is not None:
                    return result
    return get_match(product, PRODUCT_GROUPING, [])

def get_lineage_taxids_from_species(species: str) -> list:
    try:
        taxid = NCBI.get_name_translator([species])[species][0]
        lineage = NCBI.get_lineage(taxid)
        return lineage
    except KeyError:
        return []


def split_lineages(identifiers, plant_species_dict):
    """
    Makes a dictionary of lineage IDs to a set of identifiers belonging in that lineage
    Parameters
    ----------
    identifiers: list of UniProt IDs
    plant_species_dict: dict of UniProt ID to species name

    Returns
    -------
    dict
    """
    clades = defaultdict(set)
    for i, identifier in enumerate(identifiers):
        if i % 20 == 0:
            print(i)
        lineage = list(NCBI.get_taxid_translator(
            get_lineage_taxids_from_species(
                plant_species_dict[identifier]
            )
        ).values())
        for lin in lineage:
            clades[lin].add(identifier)
    return clades


def split_clades(identifiers, clades):
    dicots = {k for k in identifiers if k in clades["Gunneridae"] or k in clades["Magnoliidae"]}
    monocots = {k for k in identifiers if k in clades["Liliopsida"]}
    conifers = {k for k in identifiers if k in clades["Acrogymnospermae"]}
    rest = {k for k in identifiers if k not in dicots and k not in monocots and k not in conifers}
    return dicots, monocots, conifers, rest


def get_motif_positions_from_sequence(sequence, compiled_patterns=PATTERNS):
    """
    Find start and end indices of a list of (re compiled) motifs in a sequence

    Parameters
    ----------
    sequence
    compiled_patterns

    Returns
    -------
    list of (start_index, end_index) for each motif
    (-1, -1) if motif not found
    """
    indices = []
    found_till = 0
    sequence = sequence.upper()
    for p, pattern in enumerate(compiled_patterns):
        match = pattern.search(sequence[found_till:])
        if match is None:
            match = pattern.search(sequence)
            if match is not None:
                for i in range(p):
                    indices[i] = (-1, -1)
                indices.append((match.start(), match.end()))
                found_till += match.end()
            else:
                indices.append((-1, -1))
        else:
            indices.append((match.start() + found_till, match.end() + found_till))
            found_till += match.end()
    return indices


def get_features_dict(features_dir, keys, num_models=5, sequence_only=False):
    features_dict = defaultdict(list)
    features_dir = Path(features_dir)
    for key in keys:
        sequence_file = features_dir / f"{key}_sequence.pkl"
        if sequence_file.exists():
            with open(sequence_file, "rb") as f:
                features_dict[key].append(pickle.load(f))
        else:
            print(f"sequence file not found for key {key}")
            continue
        if not sequence_only:
            for m in range(num_models):
                str_file = features_dir / f"{key}_{m}.pkl"
                if str_file.exists():
                    with open(str_file, "rb") as f:
                        features_dict[key].append(pickle.load(f))
                else:
                    print(f"structure file {m} not found for key {key}")
    if not sequence_only:
        return {k: features_dict[k] for k in features_dict if len(features_dict[k]) == num_models + 1}
    else:
        return {k: features_dict[k] for k in features_dict if len(features_dict[k]) == 1}


def _make_aln_features_x(key, features_x: np.ndarray, aligned_sequence: str):
    """
    Makes aligned matrix of features according to an aligned sequence

    Parameters
    ----------
    features_x
    aligned_sequence

    Returns
    -------
    matrix
    """
    mask = np.array([i for i in range(len(aligned_sequence)) if aligned_sequence[i] != '-'])
    if features_x.ndim == 2:
        assert len(aligned_sequence.replace('-', '')) == features_x.shape[1], f"{key} {features_x.shape[1]}"
        if features_x.shape[0] == features_x.shape[1]:
            aln_features = np.zeros((len(aligned_sequence), len(aligned_sequence)))
            aln_features[:] = np.nan
            aln_features[np.ix_(mask, mask)] = features_x.T
        else:
            aln_features = np.zeros((len(aligned_sequence), features_x.shape[0]))
            aln_features[:] = np.nan
            aln_features[mask, :] = features_x.T
        aln_features = aln_features.reshape((1, aln_features.shape[0] * aln_features.shape[1]))
    else:
        assert len(aligned_sequence.replace('-', '')) == features_x.shape[0], f"{key} {features_x.shape[0]}"
        aln_features = np.zeros((1, len(aligned_sequence)))
        aln_features[:] = np.nan
        aln_features[:, mask] = features_x
    return aln_features


def _get_feature_shapes(structure_feature_names: list, sequence_feature_names: list, features_dict: dict, first_id: str, aln_length: int):
    feature_shapes = {}
    for feature in structure_feature_names:
        example_data = features_dict[first_id][1][feature]
        if isinstance(example_data, list) or isinstance(example_data, str) or example_data.ndim == 1:
            feature_shapes[feature] = 1
        else:
            if example_data.shape[0] == example_data.shape[1]:
                feature_shapes[feature] = aln_length
            else:
                feature_shapes[feature] = example_data.shape[0]
    for feature in sequence_feature_names:
        example_data = features_dict[first_id][0][feature]
        if isinstance(example_data, list) or isinstance(example_data, str) or example_data.ndim == 1:
            feature_shapes[feature] = 1
        else:
            if example_data.shape[0] == example_data.shape[1]:
                feature_shapes[feature] = aln_length
            else:
                feature_shapes[feature] = example_data.shape[0]
    return feature_shapes


def get_aligned_features(identifiers, features_dict: dict, aln_sequences: dict, num_models, ignore_amino=False) -> dict:
    """
    Aligns features according to an alignment

    Parameters
    ----------
    identifiers
    features_dict
    aln_sequences
    num_models

    Returns
    -------
    dict of {feature: matrix of values with num rows = num identifiers}
    """

    def encode_string_features(features_x: str, mapping: dict):
        return np.array([mapping[x] if x in mapping else len(mapping) for x in features_x])

    sequence_feature_names = list(features_dict[identifiers[0]][0].keys())
    if len(features_dict[identifiers[0]]) > 1:
        structure_feature_names = [x for x in features_dict[identifiers[0]][1].keys() if x != 'dssp_resnum']
    else:
        structure_feature_names = []
    aln_length = len(aln_sequences[identifiers[0]])
    feature_shapes = _get_feature_shapes(structure_feature_names, sequence_feature_names, features_dict, identifiers[0], aln_length)
    x_dict = {}
    for feature in structure_feature_names:
        for model_id in range(num_models):
            x_dict[f"{feature}:{model_id}"] = np.zeros((len(identifiers), aln_length * feature_shapes[feature]))
            for i in range(len(identifiers)):
                farray = features_dict[identifiers[i]][model_id + 1][feature]
                if "fluctuation" in feature:
                    farray = farray / np.nansum(farray ** 2) ** 0.5
                x_dict[f"{feature}:{model_id}"][i] = _make_aln_features_x(
                    identifiers[i],
                    farray,
                    aln_sequences[identifiers[i]])
    for feature in sequence_feature_names:
        if ignore_amino and feature == 'amino_acids':
            continue
        x_dict[feature] = np.zeros((len(identifiers), aln_length * feature_shapes[feature]))
        for i in range(len(identifiers)):
            feature_data = features_dict[identifiers[i]][0][feature]
            if feature in STRING_MAPPINGS:
                feature_data = encode_string_features(feature_data, STRING_MAPPINGS[feature])
            x_dict[feature][i] = _make_aln_features_x(
                identifiers[i],
                np.array(feature_data),
                aln_sequences[identifiers[i]])
    return x_dict


def get_conserved_residue_indices(aln_sequences, train_keys, discard_threshold: float = 0.5):
    """
    Finds indices of positions in the an alignment (aln_sequences) which have more than discard_threshold percentage
    of train_keys with non-gap residues.
    """
    residue_indices = []
    for i in range(len(aln_sequences[train_keys[0]])):
        if sum(1 for key in train_keys if aln_sequences[key][i] == '-') / len(train_keys) < discard_threshold:
            residue_indices.append(i)
    return np.array(residue_indices)


def get_onehots(num_columns):
    return {k: preprocessing.OneHotEncoder(categories=[list(range(len(STRING_MAPPINGS[k]) + 1))] * num_columns) for k in STRING_MAPPINGS}


def get_metrics(test_labels, test_proba, threshold=0.5, pred=False):
    if not pred:
        test_pred = test_proba[:, 1] > threshold
        precision, recall, thresholds_pr = metrics.precision_recall_curve(test_labels, test_proba[:, 1])
        scores = {
            "f1": metrics.f1_score(test_labels, test_pred),
            "accuracy": metrics.accuracy_score(test_labels, test_pred),
            "balanced_accuracy": metrics.balanced_accuracy_score(test_labels, test_pred),
            "roc_auc": metrics.roc_auc_score(test_labels, test_proba[:, 1]),
            "precision_recall_auc": metrics.auc(recall, precision)
        }
    else:
        test_pred = test_proba
        precision, recall, thresholds_pr = metrics.precision_recall_curve(test_labels, test_pred)
        scores = {
            "f1": metrics.f1_score(test_labels, test_pred),
            "accuracy": metrics.accuracy_score(test_labels, test_pred),
            "balanced_accuracy": metrics.balanced_accuracy_score(test_labels, test_pred),
            "roc_auc": metrics.roc_auc_score(test_labels, test_pred),
            "precision_recall_auc": metrics.auc(recall, precision)
        }

    return [np.round(scores[s_name], 3) for s_name in SCORE_NAMES] + [metrics.confusion_matrix(test_labels, test_pred)]


class Data:
    def __init__(self,
                 train_indices,
                 test_indices,
                 residue_indices,
                 feature_names,
                 ):
        self.train_indices = train_indices
        self.test_indices = test_indices
        self.train_test_split = None
        self.residue_indices = np.array(residue_indices)
        self.feature_names = feature_names
        self.feature_residue_scores = {}
        self.residue_scores_folds = None
        self.residue_scores = None
        self.sorted_residues = None
        self.feature_cv_indices = []
        self.feature_cv_predictions = defaultdict(list)
        self.feature_test_predictions = defaultdict(list)
        self.clf = None
        self.y_proba = None
        self.y_test = None

    def get_residue_scores(self):
        assert len(self.feature_residue_scores)
        self.residue_scores_folds = normalize_matrix(np.array([np.sum(self.feature_residue_scores[f], axis=0) for f in self.feature_residue_scores]))
        self.residue_scores = np.sum(self.residue_scores_folds, axis=0)
        self.sorted_residues = self.residue_indices[np.argsort(-self.residue_scores)]


@nb.njit
def normalize_matrix(matrix):
    matrix_s = np.zeros(matrix.shape)
    for i in range(matrix.shape[0]):
        matrix_s[i] = normalize(matrix[i])
    return matrix_s


@nb.njit
def normalize(row):
    maxv, minv = np.max(row), np.min(row)
    return (row-maxv)/(maxv-minv)


def get_clf(num_cols,
            learning_rate=0.005,
            n_estimators=2000,
            max_depth=5,
            min_child_weight=2,
            gamma=0.01,
            max_delta_step=0,
            subsample=0.7,
            colsample_bytree=0.1,
            colsample_bylevel=0.1,
            colsample_bynode=1,
            verbosity: int = 0,
            base_score=0.5):
    if verbosity > 1:
        assert num_cols is not None
        print("Number of columns", num_cols,
              int(colsample_bytree * num_cols),
              int(colsample_bytree * colsample_bylevel * num_cols),
              int(colsample_bytree * colsample_bylevel * colsample_bynode * num_cols))
    return XGBClassifier(
        learning_rate=learning_rate,
        base_score=base_score,
        n_estimators=n_estimators,
        max_depth=max_depth,
        min_child_weight=min_child_weight,
        gamma=gamma,
        max_delta_step=max_delta_step,
        subsample=subsample,
        colsample_bytree=colsample_bytree,
        colsample_bylevel=colsample_bylevel,
        colsample_bynode=colsample_bynode,
        reg_alpha=0,
        reg_lambda=1,
        missing=None,
        objective='binary:logistic',
        importance_type='weight',
        scale_pos_weight=1,
        nthread=40,
        seed=42)


class TerpeneTrees:
    def __init__(self,
                 aln_features_dict,
                 num_residues,
                 keys,
                 labels,
                 model_feature_names,
                 num_models=3):
        self.aln_features_dict = aln_features_dict
        self.num_residues = num_residues
        self.onehots = get_onehots(num_residues)
        self.model_feature_names = set(model_feature_names)
        self.num_models = num_models
        self.aln_features_dict_expanded = self.expand_features()
        self.keys = keys
        self.labels = labels
        self.color_mapper = {
            'F-acyclic': '#fcbba1',
            'F-10,1': '#fb6a4a',
            'F-11,1': '#cb181d',
            'N-acyclic': '#c6dbef',
            'N-6,1': '#4292c6',
            'N-11,1': '#084594',
            'cadalanes': 'black',
            'multiple': 'white'
        }
        self.marker_mapper = {
            'F': 's',
            'N': 'D',
            'cadalanes': 'o',
            'multiple': '^'
        }

    def fit_and_predict(self,
            data: Data,
            num_folds_residue_scores: int = 3,
            test_size_residue_scores: float = 0.1,
            verbosity: int = 0,
            top_num_residues: int = 30):
        data.train_test_split = self.split_train_test(data.train_indices, data.test_indices)
        self.get_residue_scores_per_feature(data, num_folds=num_folds_residue_scores, test_size=test_size_residue_scores, verbosity=verbosity)
        top_residues = data.sorted_residues[:top_num_residues]
        train_x, y_train, test_x, y_test = self.split_train_test_expanded(top_residues, data.feature_names, data.train_test_split)
        data.clf = get_clf(num_cols=train_x.shape[1])
        data.clf.fit(train_x, y_train)
        data.y_proba = data.clf.predict_proba(test_x)
        data.y_test = y_test

    def fit_and_predict_from_res(self, data: Data, top_residues: list):
        data.train_test_split = self.split_train_test(data.train_indices, data.test_indices)
        train_x, y_train, test_x, y_test = self.split_train_test_expanded(top_residues, data.feature_names, data.train_test_split)
        data.clf = get_clf(num_cols=train_x.shape[1])
        data.clf.fit(train_x, y_train)
        data.y_proba = data.clf.predict_proba(test_x)
        data.y_test = y_test

    def get_residue_scores_per_feature(self,
                                       data: Data,
                                       num_folds=1,
                                       test_size=0.05,
                                       verbosity=0):
        assert data.train_test_split is not None
        if num_folds > 1:
            folder = model_selection.StratifiedShuffleSplit(n_splits=num_folds, random_state=42, test_size=test_size)
            data.feature_cv_indices = list(folder.split(X=np.zeros((len(data.train_indices), 2)),
                                                        y=np.zeros(len(data.train_indices))))
        else:
            data.feature_cv_indices = [(np.arange(len(data.train_indices)), np.arange(len(data.test_indices)))]
        group_feature_names = defaultdict(list)
        for feature_name in data.feature_names:
            if feature_name.split('_')[-1] in {'max', 'min', 'mean', 'ca', 'cb'}:
                group_feature_names['_'.join(feature_name.split('_')[:-1])].append(feature_name)
            else:
                group_feature_names[feature_name].append(feature_name)
        for feature_name_prefix in group_feature_names:
            data.feature_residue_scores[feature_name_prefix] = np.zeros((num_folds, len(data.residue_indices)))
            if verbosity > 0:
                print(feature_name_prefix)
            train_x, y_train, test_x, y_test = self.split_train_test_expanded(data.residue_indices, group_feature_names[feature_name_prefix],
                                                                              data.train_test_split)
            num_cols = sum(self.get_num_cols(feature_name) for feature_name in group_feature_names[feature_name_prefix])
            clf = get_clf(learning_rate=0.1,
                          n_estimators=100,
                          max_depth=3,
                          min_child_weight=1,
                          max_delta_step=0,
                          gamma=0,
                          subsample=1,
                          colsample_bytree=1,
                          colsample_bylevel=1,
                          verbosity=verbosity,
                          num_cols=num_cols * len(data.residue_indices))
            test_scores = np.zeros((num_folds, len(SCORE_NAMES)))
            for f, (fold_train_indices, fold_test_indices) in enumerate(data.feature_cv_indices):
                clf.fit(train_x[fold_train_indices], y_train[fold_train_indices])
                index = 0
                for fname in group_feature_names[feature_name_prefix]:
                    f_num_cols = self.get_num_cols(fname)
                    for i, r in enumerate(data.residue_indices):
                        data.feature_residue_scores[feature_name_prefix][f, i] += np.nansum(
                            clf.feature_importances_[index + i * f_num_cols: index + (i + 1) * f_num_cols])
                    index += f_num_cols * len(data.residue_indices)
                if verbosity > 1:
                    y_pred_test = clf.predict_proba(test_x)
                    data.feature_test_predictions[feature_name_prefix].append((y_test, y_pred_test))
                    indices = np.where(y_test != -1)
                    test_scores[f] = get_metrics(y_test[indices], y_pred_test[indices])[:-1]
            if verbosity > 1:
                print("Test")
                for s, score_name in enumerate(SCORE_NAMES):
                    print(f"{score_name}: {np.round(np.mean(test_scores[:, s]), 3)} +/- {np.round(np.std(test_scores[:, s]), 3)}")
                print()
        data.get_residue_scores()

    def get_inds(self, fname, residue_indices: list, expanded=True):
        if fname in self.onehots:
            if expanded:
                n_values = self.onehots[fname].categories_[0].shape[0]
            else:
                n_values = 1
        elif "pssm" in fname or "psfm" in fname:
            n_values = 20
        elif fname in self.model_feature_names:
            n_values = self.num_models
        else:
            n_values = 1
        inds = []
        for r in residue_indices:
            inds += list(range(r * n_values, (r + 1) * n_values))
        return inds

    def get_num_cols(self, fname):
        if fname in self.onehots:
            return self.onehots[fname].categories_[0].shape[0]
        elif "pssm" in fname or "psfm" in fname:
            return 20
        elif fname in self.model_feature_names:
            return self.num_models
        else:
            return 1

    def expand_features(self):
        expanded_dict = {}
        for feature_name in self.aln_features_dict:
            if feature_name in self.onehots:
                expanded_dict[feature_name] = np.array(self.aln_features_dict[feature_name])
                expanded_dict[feature_name][np.isnan(expanded_dict[feature_name])] = self.onehots[feature_name].categories[0][-1]
                expanded_dict[feature_name] = expanded_dict[feature_name].astype(np.int32)
                expanded_dict[feature_name] = self.onehots[feature_name].fit_transform(expanded_dict[feature_name]).todense()
            elif feature_name.split(':')[0] in self.model_feature_names:
                base_name = feature_name.split(':')[0]
                expanded_dict[base_name] = np.zeros(
                    (self.aln_features_dict[feature_name].shape[0], self.aln_features_dict[feature_name].shape[1] * self.num_models))
                index = 0
                for r in range(self.aln_features_dict[feature_name].shape[1]):
                    for m in range(self.num_models):
                        expanded_dict[base_name][:, index] = self.aln_features_dict[f"{base_name}:{m}"][:, r]
                        index += 1
            else:
                expanded_dict[feature_name] = self.aln_features_dict[feature_name]
        return expanded_dict

    def split_train_test(self, train_indices, test_indices):
        train_data = {}
        test_data = {}
        for feature in self.aln_features_dict_expanded:
            train_data[feature] = self.aln_features_dict_expanded[feature][train_indices]
            test_data[feature] = self.aln_features_dict_expanded[feature][test_indices]
        y_train = self.labels[train_indices]
        y_test = self.labels[test_indices]
        return train_data, y_train, test_data, y_test

    def split_train_test_expanded(self, residue_indices, feature_names, train_test_split):
        train_data, y_train, test_data, y_test = train_test_split
        num_cols = sum(self.get_num_cols(fname) * len(residue_indices) for fname in feature_names)
        train_x = np.zeros((len(y_train), num_cols))
        test_x = np.zeros((len(y_test), num_cols))
        index = 0
        for feature_name in feature_names:
            inds = self.get_inds(feature_name, residue_indices)
            num_cols_fname = len(inds)
            train_x[:, index: index + num_cols_fname] = train_data[feature_name][:, inds]
            test_x[:, index: index + num_cols_fname] = test_data[feature_name][:, inds]
            index += num_cols_fname
        return train_x, y_train, test_x, y_test

    def get_kernel_pca(self, train_keys: list, feature_names: list, residue_indices: list):
        train_test_split = self.split_train_test(list(range(len(train_keys))), [])
        train_x, _, _, _ = self.split_train_test_expanded(residue_indices, feature_names, train_test_split)
        print(train_x.shape)
        scaler = preprocessing.MinMaxScaler()
        tx = np.nan_to_num(scaler.fit_transform(np.nan_to_num(train_x)))
        reducer = PCA(n_components=10, random_state=42)  # , kernel='linear')#kernel='rbf', gamma=6e-5)
        red_x = reducer.fit_transform(tx)
        return red_x, reducer

    def plot_kernel_pca(self, red_x: np.ndarray, train_keys: list, key_to_products: dict, key_to_clade: dict, plot_clade_size=True):
        colors = []
        markers = []
        if plot_clade_size:
            clade_to_size = {
                'dicots': 50,
                'monocots': 150,
                'conifers': 250
            }
        else:
            clade_to_size = {
                'dicots': 100,
                'monocots': 100,
                'conifers': 100
            }
        for key in train_keys:
            graph = Counter(["-".join(get_product_group_graph(p)[:2]) for p in key_to_products[key]])
            cation = Counter([g.split('-')[0] for g in graph])
            if len(graph) == 1:
                colors.append(self.color_mapper[list(graph.keys())[0]])
                markers.append(self.marker_mapper[list(cation)[0]])
            else:
                colors.append(self.color_mapper['multiple'])
                markers.append(self.marker_mapper['multiple'])

        plt.rc('font', family='serif', serif='Times', size=15)
        plt.rc('text', usetex=False)
        plt.rc('xtick', labelsize=8)
        plt.rc('ytick', labelsize=8)
        plt.rc('axes', labelsize=8)
        width = 15
        height = 15
        plt.figure(figsize=(width, height))
        plt.axis('off')
        for marker in self.marker_mapper.values():
            indices = [i for i, m in enumerate(markers) if m == marker]
            plt.scatter(red_x[indices, 0],
                        red_x[indices, 1],
                        c=[colors[i] for i in indices],
                        marker=marker,
                        alpha=0.9,
                        s=[clade_to_size[key_to_clade[train_keys[i]]] for i in indices],
                        linewidths=0.5,
                        edgecolors='black')
        legend_lines = []
        for color in self.color_mapper:
            legend_lines.append(
                plt.scatter([], [], label=color, marker=self.marker_mapper[color.split('-')[0]],
                            c=self.color_mapper[color], alpha=0.9,
                            s=100,
                            linewidth=0.5,
                            edgecolors='black'))
        plt.legend(handles=legend_lines)

    def plot_kernel_pca_plotly(self, red_x: np.ndarray, train_keys: list, key_to_products: dict, key_to_species: dict):
        data = []
        plotly_marker_mapping = {'s': 'square', 'D': 'diamond', '^': 'triangle-up', 'o': 'circle'}
        colors = []
        markers = []
        for key in train_keys:
            graph = Counter(["-".join(get_product_group_graph(p)[:2]) for p in key_to_products[key]])
            cation = Counter([g.split('-')[0] for g in graph])
            if len(graph) == 1:
                colors.append(self.color_mapper[list(graph.keys())[0]])
                markers.append(self.marker_mapper[list(cation)[0]])
            else:
                colors.append(self.color_mapper['multiple'])
                markers.append(self.marker_mapper['multiple'])
        for marker in self.marker_mapper.values():
            indices = [i for i, m in enumerate(markers) if m == marker]
            data.append(go.Scatter(x=red_x[indices, 0],
                                   y=red_x[indices, 1],
                                   mode='markers',
                                   marker=dict(
                                       size=10,
                                       symbol=plotly_marker_mapping[marker],
                                       color=[colors[x] for x in indices],
                                       line=dict(
                                           width=1,
                                       ),
                                       opacity=0.9
                                   ),
                                   text=[train_keys[x] + "\n" + key_to_species[train_keys[x]] for x in indices],
                                   ))

        layout = go.Layout(
            width=800,
            height=800,
            xaxis={"ticks": "", "showticklabels": False, 'showline': False, 'zeroline': False, 'showgrid': False},
            yaxis={"ticks": "", "showticklabels": False, 'showline': False, 'zeroline': False, 'showgrid': False},
        )
        return go.Figure(data, layout=layout)
