## Integrating structure-based machine learning and co-evolution to investigate specificity in plant sesquiterpene synthases

### code (in sts\_cation\_prediction)
* homology\_modelling.py and parallel\_model.py - to make multi-template homology models in parallel using Modeller.
* make\_data.py - functions for extracting sequence and structure features from homology models in parallel (uses sequence\_feature\_extraction.py and structure\_feature\_extraction.py)
* prediction.py - XGBoost-based hierarchical cation classifier
* helper.py - additional helper functions for input, output, HMMs, alignment etc. 

### data:
* models\_c\_one - homology models of the C-terminal domains of characterized STSs using a single template
* models\_c\_six - multi-template homology models of the C-terminal domain, built using six templates
* models\_n\_c\_six - multi-template homology models of the full (N- and C-terminal domain), built using six templates
* coevolution\_alignment.fasta - an alignment of 8344 putative terpene synthases
* coevolution\_alignment.mat - the predicted contact matrix returned by CCMPred on the above alignment
